### Kako to da je vse 'commitano' v tako kratkem času? Hmm... sumljivo! ###

Razlog za to je, da sem pozabil pognati začetne ukaze iz navodil, tako da nisem imel uveljavitev, ki ozančujejo začetek dela na posamezni nalogi. Prav tako pa sem imel težave pri združevanju vej in sem se odločil, da zbrišem star repozitorij in na vsako vejo shranim identično kodo - to sem naredil v desetih minutah.