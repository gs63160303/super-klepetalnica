/**
 * Zamenjava kode smeška s sliko (iz oddaljenega strežnika
 * https://sandbox.lavbic.net/teaching/OIS/gradivo/{smesko}.png)
 * 
 * @param vhodnoBesedilo sporočilo, ki lahko vsebuje kodo smeška
 */
function dodajSmeske(vhodnoBesedilo) {
  var preslikovalnaTabela = {
    ";)": "wink.png",
    ":)": "smiley.png",
    "(y)": "like.png",
    ":*": "kiss.png",
    ":(": "sad.png"
  }
  for (var smesko in preslikovalnaTabela) {
    vhodnoBesedilo = vhodnoBesedilo.split(smesko).join(
      "<img src='https://sandbox.lavbic.net/teaching/OIS/gradivo/" +
      preslikovalnaTabela[smesko] + "' />");
  }
  return vhodnoBesedilo;
}


/**
 * Čiščenje besedila sporočila z namenom onemogočanja XSS napadov
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementEnostavniTekstSporocila(sporocilo) {
  
  var jeKrcanje = sporocilo.indexOf("&#9756") > -1 && sporocilo.indexOf("(zasebno): &#9756") > -1 &&
                  sporocilo.indexOf("&#9756") == (sporocilo.length - "&#9756".length) &&
                  sporocilo.indexOf('<') == -1;
                  
                  
  jeKrcanje = false;
                  
  if (jeKrcanje) {
    var splitano = sporocilo.split(" ");
    sporocilo = splitano[0] + " " + splitano[1] + " &#9756";
    return divElementHtmlTekst(sporocilo);
  } else {
    
    sporocilo = 
      sporocilo.split("<").join("&lt;")
               .split(">").join("&gt;")
               .split("&lt;img").join("<img")
               .split("png' /&gt;").join("png' />");
  
  var besede = sporocilo.split(" ");
  besede.push("<br />");
  /*
  
  var start = ["http"];
  var end = [".jpg", ".gif", ".png"];
  var len = end.length;
  for (var j = 0; j < len; j++) {
      end.push(end[j] + '.');
      end.push(end[j] + '?');
      end.push(end[j] + '!');
  }
  
  
  
  for (var i = 0; i < besede.length; i++) {
    var zac = 0;
    var kon = 0;
    var starts = false;
    for (var j = 0; j < start.length; j++) {
      starts = besede[i].startsWith(start[j]);
      if(starts) {
        
        break;
      }
    }
    var ends = false;
    for (var j = 0; j < end.length; j++) {
      ends = besede[i].endsWith(end[j]);
      if(ends) break;
    }
    //var jeSlika = besede[i].toLowerCase().startsWith("http") && 
    //              (besede[i].toLowerCase().endsWith(".jpg") || besede[i].toLowerCase().endsWith(".png") || besede[i].toLowerCase().endsWith(".gif"));
    var jeSlika = starts && ends;
    if (jeSlika) {
      besede.push("<img style='width: 200px; padding-left: 20px;' src='" + besede[i] + "'><br />");
    }
  }*/
  
  for (var i = 0; i < besede.length; i++) {
    var jeSlika = besede[i].toLowerCase().startsWith("http") && 
                  (besede[i].toLowerCase().endsWith(".jpg") || besede[i].toLowerCase().endsWith(".png") || besede[i].toLowerCase().endsWith(".gif"));
    if (jeSlika) {
      besede.push("<img style='width: 200px; padding-left: 20px;' src='" + besede[i] + "'><br />");
    } else {
    
      jeSlika = besede[i].toLowerCase().startsWith("http") && 
                (besede[i].toLowerCase().endsWith(".jpg.") || besede[i].toLowerCase().endsWith(".png.") || besede[i].toLowerCase().endsWith(".gif.") ||
                besede[i].toLowerCase().endsWith(".jpg?") || besede[i].toLowerCase().endsWith(".png?") || besede[i].toLowerCase().endsWith(".gif?") ||
                besede[i].toLowerCase().endsWith(".jpg!") || besede[i].toLowerCase().endsWith(".png!") || besede[i].toLowerCase().endsWith(".gif!"));
                
      if (jeSlika) {
        besede.push("<img style='width: 200px; padding-left: 20px;' src='" + besede[i].substring(0, besede[i].length - 1) + "'><br />");
        //console.log("je");
    }
    }
    
    
  }
  
  sporocilo = besede.join(' ');
  
  return $('<div style="font-weight: bold"></div>').html(sporocilo);
    
  }
}

/**
 * Čiščenje besedila sporočila z namenom onemogočanja XSS napadov
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementEnostavniTekst(sporocilo) {
  
  
  var jeSmesko = sporocilo.indexOf("https://sandbox.lavbic.net/teaching/OIS/gradivo/") > -1;
  if (jeSmesko) {
    sporocilo = 
      sporocilo.split("<").join("&lt;")
               .split(">").join("&gt;")
               .split("&lt;img").join("<img")
               .split("png' /&gt;").join("png' />");
    return divElementHtmlTekst(sporocilo);
  } else {
    return $('<div style="font-weight: bold"></div>').text(sporocilo);  
  }
  
}


/**
 * Prikaz "varnih" sporočil, t.j. tistih, ki jih je generiral sistem
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}


/**
 * Obdelaj besedilo, ki ga uporabnik vnese v obrazec na spletni strani, kjer
 * je potrebno ugotoviti ali gre za ukaz ali za sporočilo na kanal
 * 
 * @param klepetApp objekt Klepet, ki nam olajša obvladovanje 
 *        funkcionalnosti uporabnika
 * @param socket socket WebSocket trenutno prijavljenega uporabnika
 */
function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  sporocilo = dodajSmeske(sporocilo);
  var sistemskoSporocilo;
  
  // Če uporabnik začne sporočilo z znakom '/', ga obravnavaj kot ukaz
  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementEnostavniTekstSporocila(sistemskoSporocilo));
      $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
    }
  // Če gre za sporočilo na kanal, ga posreduj vsem članom kanala
  } else {
    sporocilo = filtirirajVulgarneBesede(sporocilo);
    klepetApp.posljiSporocilo(trenutniKanal, sporocilo);
    $('#sporocila').append(divElementEnostavniTekstSporocila(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

// Branje vulgarnih besed v seznam
var vulgarneBesede = [];
$.get('/swearWords.txt', function(podatki) {
  vulgarneBesede = podatki.split('\r\n'); 
});

/**
* Iz podanega niza vse besede iz seznama vulgarnih besed zamenjaj z enako dolžino zvezdic (*).
* 
* @param vhodni niz
*/
function filtirirajVulgarneBesede(vhod) {

 for (var i in vulgarneBesede) {
   var re = new RegExp('\\b' + vulgarneBesede[i] + '\\b', 'gi');
   vhod = vhod.replace(re, "*".repeat(vulgarneBesede[i].length));
 }
 
 return vhod;
}


var socket = io.connect();
var trenutniVzdevek = "";
var trenutniKanal = "";
var users = {};

// Poačakj, da se celotna stran naloži, šele nato začni z izvajanjem kode
$(document).ready(function() {
  var klepetApp = new Klepet(socket);
  
  // Prikaži rezultat zahteve po spremembi vzdevka
  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      trenutniVzdevek = rezultat.vzdevek;
      $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  });
  
  // Prikaži rezultat zahteve po spremembi kanala
  socket.on('pridruzitevOdgovor', function(rezultat) {
    trenutniKanal = rezultat.kanal;
    $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  });
  
  // Prikaži prejeto sporočilo
  socket.on('sporocilo', function (sporocilo) {
    var msg = sporocilo.besedilo.split(':');
    
    
    var tmp = nadimekPlusUporabnik(msg[0]);
    if (tmp) {
      msg[0] = tmp;
    } else {
      var tmp = msg[0].split(" (zasebno)");
      tmp2 = nadimekPlusUporabnik(tmp[0]);
      if (tmp2) {
        tmp[0] = tmp2;
      }
      msg[0] = tmp.join(" (zasebno)");
    }
    
    msg = msg.join(':');
    var novElement = divElementEnostavniTekstSporocila(msg);
    $('#sporocila').append(novElement);
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
    
    /*
    var spremenilVzdevek = sporocilo.besedilo.indexOf(' se je preimenoval v ') > -1;
    if(spremenilVzdevek) {
      var vzdevka = sporocilo.besedilo.split(' se je preimenoval v ');
      console.log(vzdevka[0] + "|" + vzdevka[1]);
    }*/
  });
  
  // Prikaži seznam kanalov, ki so na voljo
  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var i in kanali) {
      if (kanali[i] != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanali[i]));
      }
    }
  
    // Klik na ime kanala v seznamu kanalov zahteva pridružitev izbranemu kanalu
    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });
  
  // spremembaVzdevkaInfo
  socket.on('spremembaVzdevkaInfo', function(info) {
    var vzdevka = info.besedilo.split('|');
    var stari = vzdevka[0];
    var novi = vzdevka[1];
    var nadimek = users[stari];
    if (users[stari].localeCompare(stari) == 0) {
      nadimek = novi;
    }
    delete users[vzdevka[0]];
    users[novi] = nadimek;
    console.log(novi + " " + users[novi]);
  });
  
  // Prikaži seznam trenutnih uporabnikov na kanalu
  socket.on('uporabniki', function(uporabniki) {
    $('#seznam-uporabnikov').empty();
    var tmpUsers = {};
    for (var i=0; i < uporabniki.length; i++) {
      if (uporabniki[i] in users) {
        tmpUsers[uporabniki[i]] = users[uporabniki[i]];
      } else {
        tmpUsers[uporabniki[i]] = uporabniki[i];
      }
    }
    users = tmpUsers;
    
    for (var i=0; i < uporabniki.length; i++) {
      var toDisplay = nadimekPlusUporabnik(uporabniki[i]);
      /*
      var toDisplay = uporabniki[i];
      if (users[uporabniki[i]] != uporabniki[i]) {
        toDisplay = users[uporabniki[i]] + " (" + uporabniki[i] + ")";
      }
      */
      $('#seznam-uporabnikov').append(divElementEnostavniTekst(toDisplay));
    }
    
    // Klik na ime uporabnika v seznamu uporabniko krcne izbranega uporabnika
    $('#seznam-uporabnikov div').click(function() {
      var sistemskoSporocilo;
      var uporabnik = izlusciUporabika($(this).text());
      sistemskoSporocilo = klepetApp.procesirajUkaz('/zasebno "' + uporabnik + '" "&#9756"');
      if (sistemskoSporocilo) {
        var novoSporocilo = divElementEnostavniTekstSporocila(sistemskoSporocilo);
        $('#sporocila').append(novoSporocilo);
        $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
      }
      $('#poslji-sporocilo').focus();
    });
    
  });
  
  function izlusciUporabika(vsebina) {
    var matches = vsebina.match(/\((.*?)\)/);

      if (matches) {
          var vsebina = matches[1];
      }
      console.log(vsebina + "|");
      return vsebina;
  }
  
  function nadimekPlusUporabnik(uporabnik) {
    if (uporabnik in users) {
      if (users[uporabnik] != uporabnik) {
        uporabnik = users[uporabnik] + " (" + uporabnik + ")";
      }
      return uporabnik;
    } 
    return "";
  }
  
  function posodobiUporabnike(uporabnik) {
    for (var i = 0; i < nadimki.length; i++) {
      if (nadimki[i][0] === uporabnik && nadimki[i][0] != nadimki[i][1]) {
        // posodobi tabelo nadimkov
        return nadimki[i][1];
      }
    }
    return uporabnik;
  }
  
  // Seznam kanalov in uporabnikov posodabljaj vsako sekundo
  setInterval(function() {
    socket.emit('kanali');
    socket.emit('uporabniki', {kanal: trenutniKanal});
  }, 1000);

  $('#poslji-sporocilo').focus();
  
  // S klikom na gumb pošljemo sporočilo strežniku
  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});